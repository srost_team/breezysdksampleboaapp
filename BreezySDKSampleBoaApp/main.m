//
//  main.m
//  BreezySDKSampleBoaApp
//
//  Created by Stanislav Razbegin on 10/7/15.
//  Copyright (c) 2015 SrostStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
