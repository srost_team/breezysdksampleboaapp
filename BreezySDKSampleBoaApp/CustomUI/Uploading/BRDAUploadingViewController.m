//
//  BRDAUploadingViewController.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 26/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import "BRDAUploadingViewController.h"
#import "BreezySDK.h"

@interface BRDAUploadingViewController () <BreezySDKUploadDelegate>

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (nonatomic) BOOL isUploadingStarts;

@end

@implementation BRDAUploadingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionButtonAction:(UIButton *)sender {
    
    if ( self.isUploadingStarts ) {
        [[BreezySDK sharedInstance] stopUploading];
        return;
    }
    
    NSError *error = nil;
    if ( ![[BreezySDK sharedInstance] startUploadingWithDelegate:self statesBlock:nil error:&error]) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }
}

- (void)setIsUploadingStarts:(BOOL)isUploadingStarts {
    _isUploadingStarts = isUploadingStarts;
    
    if ( isUploadingStarts ) {
        [self.actionButton setTitle:@"Stop" forState:UIControlStateNormal];
    } else {
        [self.actionButton setTitle:@"Start" forState:UIControlStateNormal];
    }
}

#pragma mark - BreezySDKUploadDelegate

- (void)willStartUpload {
    self.statusLabel.text = @"Uploading starts";
    self.isUploadingStarts = true;
}

- (void)didChangeUploadStatus: (EBRSDKUploadStatus)status {
    NSString *statusText = @"";
    
    switch (status) {
        case eBRSDKUploadStatusPreparing:
            statusText = @"eBRSDKUploadStatusPreparing";
            break;
            
        case eBRSDKUploadStatusReleasing:
            statusText = @"eBRSDKUploadStatusReleasing";
            break;
            
        case eBRSDKUploadStatusReleased:
            statusText = @"eBRSDKUploadStatusReleased";
            break;
            
        case eBRSDKUploadStatusUploading:
            statusText = @"eBRSDKUploadStatusUploading";
            break;
            
        case eBRSDKUploadStatusUploaded:
            statusText = @"eBRSDKUploadStatusUploaded";
            break;
            
        case eBRSDKUploadStatusUploadFailed:
            statusText = @"eBRSDKUploadStatusUploadFailed";
            break;
            
        case eBRSDKUploadStatusFinalizing:
            statusText = @"eBRSDKUploadStatusFinalizing";
            break;
            
        case eBRSDKUploadStatusFinished:
            statusText = @"eBRSDKUploadStatusFinished";
            break;
            
        default:
            break;
    }
    
    self.statusLabel.text = statusText;
}

- (void)didUploadDocument {
    self.statusLabel.text = @"Uploaded";
    self.isUploadingStarts = false;
}

- (void)uploadFailedWithError:(NSError*)error {
    self.isUploadingStarts = false;
    [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
}

@end
