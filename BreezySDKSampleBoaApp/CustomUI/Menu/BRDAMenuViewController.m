//
//  BRDAMenuViewController.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 26/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import "BRDAMenuViewController.h"
#import "BRDASelectPrinterTypeViewController.h"
#import "BRDAUploadingViewController.h"
#import "BreezySDK.h"

@interface BRDAMenuViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation BRDAMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *dismissViewCotnrollerButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(close)];
    self.navigationItem.leftBarButtonItem = dismissViewCotnrollerButton;
    // Do any additional setup after loading the view from its nib.
}


- (void)viewWillAppear:(BOOL)animated {
    if ( ![[BreezySDK sharedInstance] isLoggedIn] ) {
        UIBarButtonItem *debugDeleteDataButton = [[UIBarButtonItem alloc] initWithTitle:@"Log in" style:UIBarButtonItemStyleDone target:self action:@selector(logIn)];
        self.navigationItem.rightBarButtonItem = debugDeleteDataButton;
    } else {
         self.navigationItem.rightBarButtonItem = nil;
    }
    
    [self.tableView reloadData];
}

- (void)close {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)logIn {
    [[BreezySDK sharedInstance] loginFromViewController:self error:nil success:nil failure:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray*)sectionsTitles {
    return @[@"Document", @"Printer", @"Printer settings", @"Upload"];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ( [BreezySDK sharedInstance].isLoggedIn ) {
         return 4;
    } else {
        return 0;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.sectionsTitles[section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    if ( indexPath.section == 0 ) {
        cell.textLabel.text = [self documentCellTitle];
    } else if ( indexPath.section == 1 ) {
        cell.textLabel.text = [self printerCellTitle];
    } else if ( indexPath.section == 2) {
        cell.textLabel.text = [self printerSettingsCellTitle];
        
        BOOL isEnable = ([BreezySDK sharedInstance].selectedPrinter) ? true : false;
        
        cell.textLabel.alpha = isEnable ? 1.0 : 0.5;
        [cell setUserInteractionEnabled:isEnable];
        
    } else {
        cell.textLabel.text = @"Upload";
        cell.textLabel.alpha = ( [BreezySDK sharedInstance].isPrintingPermitted ) ? 1.0 : 0.5;
        [cell setUserInteractionEnabled:[BreezySDK sharedInstance].isPrintingPermitted];
    }
    
    return cell;
}

#pragma mark - Cells

- (NSString*)documentCellTitle {
    if ( [BreezySDK sharedInstance].currentDocument ) {
        return [BreezySDK sharedInstance].currentDocument.friendlyName;
    } else {
        return @"Tap to select document";
    }
}

- (NSString*)printerCellTitle {
    if ( [BreezySDK sharedInstance].selectedPrinter ) {
        return [BreezySDK sharedInstance].selectedPrinter.printerName;
    } else {
        return @"Tap to select printer";
    }
}

- (NSString*)printerSettingsCellTitle {
    if ( ![BreezySDK sharedInstance].selectedPrinter ) {
        return @"Select printer first";
    }
    
    if ( [BreezySDK sharedInstance].currentPrinterSettings ) {
        return @"Printer settings loaded";
    } else {
        return @"Tap to load printer settings";
    }
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        NSData *testData = [@"Data" dataUsingEncoding:NSUTF8StringEncoding];
        [[BreezySDK sharedInstance] saveData:testData withFileName:@"Test document" fileExtension:@"data" success:nil failure:nil];
        [self.tableView reloadData];
        
    } else if (indexPath.section == 1) {
        
        BRDASelectPrinterTypeViewController *selectPrintersVC = [[BRDASelectPrinterTypeViewController alloc] initWithNibName:@"BRDASelectPrinterTypeViewController" bundle:nil];
        [self.navigationController pushViewController:selectPrintersVC animated:YES];
        
    } else if (indexPath.section == 2) {
        
        if ( [BreezySDK sharedInstance].currentPrinterSettings ) {
            [self.tableView reloadData];
        } else {
            [[BreezySDK sharedInstance] loadPrinterSettingsForCurrentPrinterWithSuccess:^(BRPrinterSettingsEntity *printerSettings) {
                [self.tableView reloadData];
            } failure:^(NSError *error) {
                NSLog(@"%@", error);
            }];
        }
        
    } else {
        
        BRDAUploadingViewController *selectPrintersVC = [[BRDAUploadingViewController alloc] initWithNibName:@"BRDAUploadingViewController" bundle:nil];
        [self.navigationController pushViewController:selectPrintersVC animated:YES];
        
    }
}

@end
