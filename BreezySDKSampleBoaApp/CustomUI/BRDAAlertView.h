//
//  BRDAAlertView.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 09/03/15.
//  Copyright (c) 2015 BreezyPrint Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#define BRDA_ENABLE_ALERTS YES

@interface BRDAAlertView : NSObject

+ (void)showAlertWithMessage: (NSString*)message;
+ (void)showAlertWithMessage: (NSString*)message
                    andTitle: (NSString*)title;

+ (void)showError: (NSError*)error;

+ (void)showError: (NSError*)error
      withMessage: (NSString*)message;

@end
