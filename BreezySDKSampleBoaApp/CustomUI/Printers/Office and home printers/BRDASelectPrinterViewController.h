//
//  BRDASelectPrinterViewController.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 26/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BreezySDK.h"

@interface BRDASelectPrinterViewController : UIViewController

@property (nonatomic) BRSDKPrinterSource type;

@end
