//
//  BRDASelectPrinterViewController.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 26/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import "BRDASelectPrinterViewController.h"

@interface BRDASelectPrinterViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray *printers;

@end

@implementation BRDASelectPrinterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.printers = [[BreezySDK sharedInstance] localPrintersWithType:self.type];
    
    [[BreezySDK sharedInstance] loadPrintersWithType:self.type success:^(NSArray *printers) {
        self.printers = printers;
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        NSLog(@"%@", error);
    }];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.printers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    BRPrinterEntity *printer = self.printers[indexPath.row];
    cell.textLabel.text = [printer.printerName stringByAppendingFormat:@" - %@", printer.printerId];
    cell.textLabel.adjustsFontSizeToFitWidth = true;
    
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [BreezySDK sharedInstance].selectedPrinter = self.printers[indexPath.row];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
