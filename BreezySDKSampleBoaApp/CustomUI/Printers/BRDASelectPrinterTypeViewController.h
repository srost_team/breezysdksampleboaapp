//
//  BRDASelectPrinterTypeViewController.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 26/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BRDASelectPrinterTypeViewController : UIViewController

@end
