//
//  BRDASelectPrinterTypeViewController.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 26/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import "BRDASelectPrinterTypeViewController.h"
#import "BRDASelectPrinterViewController.h"
#import "BRDAPartnerPrintersViewController.h"

@interface BRDASelectPrinterTypeViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation BRDASelectPrinterTypeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *titles = @[@"Home printers", @"Office printers", @"Partner printers"];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.textLabel.text = titles[indexPath.row];
    
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        BRDASelectPrinterViewController *selectVC = [[BRDASelectPrinterViewController alloc] initWithNibName:@"BRDASelectPrinterViewController" bundle:nil];
        selectVC.type = BRSDKHomePrintersSource;
        [self.navigationController pushViewController:selectVC animated:YES];
    } else if (indexPath.row == 1) {
        BRDASelectPrinterViewController *selectVC = [[BRDASelectPrinterViewController alloc] initWithNibName:@"BRDASelectPrinterViewController" bundle:nil];
        selectVC.type = BRSDKOfficePrintersSource;
        [self.navigationController pushViewController:selectVC animated:YES];
    } else {
        BRDAPartnerPrintersViewController *selectVC = [[BRDAPartnerPrintersViewController alloc] initWithNibName:@"BRDAPartnerPrintersViewController" bundle:nil];
        [self.navigationController pushViewController:selectVC animated:YES];
    }
}

@end
