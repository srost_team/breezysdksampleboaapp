//
//  BRSDKDemoViewController.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 18/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BRSDKDefaultUIDemoViewController : UIViewController

@property (assign, nonatomic) BOOL isDefaultLogin;

@end
