//
//  BRSDKDemoViewController.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 18/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import "BRSDKDefaultUIDemoViewController.h"
#import "BreezySDK.h"
#import "BRDALoginViewController.h"
#import "BRDAAlertView.h"

@import MapKit;

@interface BRSDKDefaultUIDemoViewController () <BreezySDKDelegate, BreezySDKUploadDelegate>

@property (weak, nonatomic) IBOutlet UITextField *documentNameTextField;
@property (nonatomic) BOOL useBasicUI;

@end

@implementation BRSDKDefaultUIDemoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.useBasicUI = true;
    
    [BreezySDK sharedInstance].delegate = self;
    
    UIBarButtonItem *dismissViewCotnrollerButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(close)];
    self.navigationItem.leftBarButtonItem = dismissViewCotnrollerButton;
}

- (void)close {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)print {
    [self createTestDocument];
    
    // Print from current controller
    NSError *error;
    BOOL success = [[BreezySDK sharedInstance] printWithBreezyUIFromViewController:self error:&error];
    
    if ( !success ) {
        [BRDAAlertView showError:error withMessage:@"Printing from current view controller error"];
    }
}

- (IBAction)printDocumentAction:(UIButton *)sender {
    if ([BreezySDK sharedInstance].isLoggedIn) {
        [self print];
    }
    else {
        if (self.isDefaultLogin) {
            [self print];
        }
        else {
            BRDALoginViewController *loginVC = [[BRDALoginViewController alloc] initWithNibName:@"BRDALoginViewController" bundle:nil];
            UINavigationController *loginVCNav = [[UINavigationController alloc] initWithRootViewController: loginVC];
            if (IS_IPAD) {
                loginVCNav.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            [self presentViewController:loginVCNav animated:YES completion:nil];
        }
    }
    
}

- (void)createTestDocument {
    if ( self.documentNameTextField.text.length > 0 ) {
        NSData *testData = [@"Data" dataUsingEncoding:NSUTF8StringEncoding];
        
        [[BreezySDK sharedInstance] saveData:testData withFileName:self.documentNameTextField.text fileExtension:@"data" success:^{
            NSLog(@"Success saving doc");
        } failure:^(NSError *error) {
            [BRDAAlertView showError:error withMessage:@"Saving document data error"];
        }];
    }
}

#pragma mark - SDK Basic Interface

/*! Method calling, when user was loging */
- (void)didLogin {
    NSLog(@"User did login");
}

/*! Method calling, when sdk requested document for save. Use SDKMethods to create document in this delegate. */
- (void)didRequestDocument {
    NSLog(@"SDK requested document");
}

/*! Method calling, when sdk has some problems with requered data */
- (void)sdkFailedWithError:(NSError*)error {
    NSLog(@"SDK Error: %@", error.localizedDescription);
}

/*! Mathod calling, when user select any printer
 * \param printer current printer
 */
- (void)didSelectPrinter:(BRPrinterEntity*)printer {
    
}

- (void)showAlertView:(NSError*)error {
    
}

#pragma mark - Uploading Delegat

- (void)willStartUpload {
    NSLog(@"Start upload");
}

/*! Mathod calling, when user select any printer
 * \param status uploading status
 */
//- (void)documentUploadingChangedToState:(EBRSUploadingStatus)state;
- (void)didChangeUploadStatus: (EBRSDKUploadStatus)status {
    NSLog(@"Change to status %i", status);
}

/*! Mathod calling, when document was printed
 * \param successful is printing was successful or not
 */
//- (void)didUpload:(BOOL)successful;

- (void)didUploadDocument {
    NSLog(@"Did upload document");
}

- (void)uploadFailedWithError:(NSError*)error {
    NSLog(@"Failed with error: %@", error);
}



@end
