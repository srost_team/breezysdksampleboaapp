//
//  BreezySDKDelegate.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 16/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BRPrinterEntity.h"
#import "BRDocumentEntity.h"

@protocol BreezySDKDelegate <NSObject>

/*! Method calling, when user was loging */
- (void)didLogin; //- (void)userDidLogin;

/*! Method calling, when sdk requested document for save. Use SDKMethods to create document in this delegate. */
- (void)savingDocumentFailedWithError:(NSError*)error; //- (void)sdkSaveDocumentFailedWithError:(NSError*)error;

/*! Method calling, when sdk has some problems with required data */
- (void)sdkFailedWithError:(NSError*)error; //- (void)sdkWorkFailedWithError:(NSError*)error;

/*! Mathod calling, when user select any printer
 * \param printer current printer
 */
-(void)didSelectPrinter:(BRPrinterEntity*)printer; //- (void)printerWasSelected:(BRPrinterEntity*)printer;

/*! Mathod calling, when document was printed
 * \param successful is printing was successful or not
 */
- (void)didUploadDocument: (BOOL)success;

@end

typedef NS_ENUM(NSInteger, EBRSDKUploadStatus) {
    eBRSDKUploadStatusPreparing = 0,
    eBRSDKUploadStatusReleasing,
    eBRSDKUploadStatusReleased,
    eBRSDKUploadStatusUploading,
    eBRSDKUploadStatusUploaded,
    eBRSDKUploadStatusUploadFailed,
    eBRSDKUploadStatusFinalizing,
    eBRSDKUploadStatusFinished,
    eBRSDKUploadStatusFailed
};


@protocol BreezySDKUploadDelegate <NSObject>

/*! Method calling, when document uploading will starts */
//- (void)documentUploadingWillStart;

- (void)willStartUpload;

/*! Mathod calling, when user select any printer
 * \param status uploading status
 */
//- (void)documentUploadingChangedToState:(EBRSUploadingStatus)state;
- (void)didChangeUploadStatus: (EBRSDKUploadStatus)status;

/*! Mathod calling, when document was printed
 * \param successful is printing was successful or not
 */
//- (void)didUpload:(BOOL)successful;

- (void)didUploadDocument;
- (void)uploadFailedWithError:(NSError*)error;

@end


/*! Executes an HTTP GET command and retrieves the information.
 * \param url The URL to perform the GET operation
 * \param userName The username to use with the request
 * \param password The password to use with the request
 * \returns The response of the request, or null if we got 404 or nothing
 */
