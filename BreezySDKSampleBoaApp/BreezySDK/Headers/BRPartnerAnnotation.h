//
//  BRPartnerAnnotation.h
//  Breezy-iOS2
//
//  Created by Igor Tudoran on 4/28/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "BRPrinterLocationEntity.h"

@interface BRPartnerAnnotation : MKPointAnnotation

@property (nonatomic) BRPrinterLocationEntity *printerLocation;

@end
