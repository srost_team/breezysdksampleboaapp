//
//  BRPrinterEntity.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 23/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BRPrintJobEntity;

@interface BRPrinterEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * isPrinterDeleted;
@property (nonatomic, retain) NSNumber * isPrinterFavorite;
@property (nonatomic, retain) NSDate * lastTimeUsed;
@property (nonatomic, retain) NSNumber * printerId;
@property (nonatomic, retain) NSString * printerName;
@property (nonatomic, retain) NSNumber * source;
@property (nonatomic, retain) NSNumber * locationId;
@property (nonatomic, retain) BRPrintJobEntity *printJob;

@end
