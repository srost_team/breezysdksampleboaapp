//
//  BreezySDK.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 16/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BreezySDKDelegate.h"
#import "BRSDKPartnerPrinter.h"
#import "BRPrinterSettingsEntity.h"
#import "BRPrinterSettingsEnums.h"

#define BR_SDK_LOGIN_BASE_URL @"http://api-v20-staging.breezy.com/"
#define BR_SDK_OAUTH_CLIENT_ID @"DP-GqheU93sSMKfG2Ao0rFhaOHcxAmSQLPoMclbhjikip1RNXsZFN1pLxNYwQmPDtMz"
#define BR_SDK_OAUTH_CLIENT_SECRET @"xr49R3XH2BkzBrv0vVTcFPazjymhTbWsfx22zaqGe9Li7yyZM8bJMPAA4OUlzMLX"

#define BR_SSO_KEY @"erez5aj5d9u4a7i5v7tqam47fxtwzdpdfgn7tvprsjyzvf49ozhy9a1wz8e78wl21dvc9qwh42k0gxqu54oxf74uzw3fcogf8e4t3nx6hrpu4d3kg7hkaysc9oqsoe7p"

typedef NS_ENUM(NSInteger, BRSDKPrinterSource) {
    BRSDKHomePrintersSource = 0,
	BRSDKOfficePrintersSource = 1,
	BRSDKPartnerPrintersSource = 2,
};

/*! NSNotification parametr contains NSNumber - printer type */
extern NSString *kNotificationPrintersWasUpdated;

@import MapKit;

@interface BreezySDK : NSObject

#pragma mark - SDK Setup

+ (BreezySDK*)sharedInstance;

+ (id)initWithBaseURL:(NSURL *)baseURL;

#pragma mark - Login properties

@property (nonatomic, readonly) BOOL isLoggedIn;

- (void)logout;


#pragma mark - Interfaces

@property (nonatomic,strong) id<BreezySDKDelegate> delegate;


#pragma mark - SDK Methods

- (BOOL)printWithBreezyUIFromViewController: (UIViewController*)parentViewController
                                      error: (NSError**)error;

- (void)dissmissBreezyUI;

#pragma mark - Document Methods

@property (nonatomic, readonly) BRDocumentEntity *currentDocument;

- (void)saveData: (NSData*)data
    withFileName: (NSString*)fileName
   fileExtension: (NSString*)fileExtension
         success: (void (^)())successBlock
         failure: (void (^)(NSError* error))failureBlock;

- (void)saveFileFromPath: (NSString*)filePath
                 success: (void (^)())successBlock
                 failure: (void (^)(NSError* error))failureBlock;

- (void)saveFileFromURL: (NSURL*)fileURL
                success: (void (^)())successBlock
                failure: (void (^)(NSError* error))failureBlock;

- (void)removeDocument;

#pragma mark - Printers

@property (nonatomic) BRPrinterEntity *selectedPrinter;

/* Upon completion of this method printers loaded from the server will end up in the local storage. They can be accessed later on using localPrintersWithType: method
 */
- (void)loadPrintersWithType: (BRSDKPrinterSource)type
                     success: (void (^)(NSArray *printers))success
                     failure: (void (^)(NSError* error))failure;

- (NSArray*)localPrintersWithType: (BRSDKPrinterSource)type;


/* NSArray *arrayOfPartnerPrinters contain BRSDKPartnerPrinter objects
 */
- (void)requestPartnerPrintersWithLocation: (CLLocation *)location
                                    radius: (CGFloat)radius
                                completion: (void (^)(BOOL success, NSArray *arrayOfPartnerPrinters))completion;

#pragma mark - Printer Settings

@property (nonatomic) BRPrinterSettingsEntity *currentPrinterSettings;

- (void)loadPrinterSettingsForCurrentPrinterWithSuccess:(void (^)(BRPrinterSettingsEntity *printerSettings))success
                                                failure: (void (^)(NSError* error))failure;

#pragma mark - Methods For Custom UI

- (BOOL)loginFromViewController: (UIViewController*)parentViewController
                          error: (NSError**)error
                        success: (void (^)())success
                        failure: (void (^)(NSError* error))failure;

- (void)loginWithEmail: (NSString*)email password: (NSString*)password
               success: (void (^)())success
               failure: (void (^)(NSError *error))failure;

- (void)loginSSOWithEmail: (NSString*)email userGroups: (NSArray*)groups authKey: (NSString*)key
                  success: (void (^)())success
                  failure: (void (^)(NSError *error))failure;

#pragma mark - Interface For Custom UI

@property (nonatomic,strong) id<BreezySDKUploadDelegate> uploadDelegate;

#pragma mark - Custom Upload

/*! Param shows whether printing is enabled to the user based on availibility of the document, printer and current permissions. It can be used to enable/disable printing button in the client code */
@property (nonatomic, readonly) BOOL isPrintingPermitted; 


- (BOOL)startUploadingWithDelegate: (id<BreezySDKUploadDelegate>)uploadDelegate
                       statesBlock: (void (^)(EBRSDKUploadStatus state))statesBlock
                             error: (NSError**)error;
- (void)stopUploading;

@end
