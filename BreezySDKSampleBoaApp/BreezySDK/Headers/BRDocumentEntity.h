//
//  BRDocumentEntity.h
//  Breezy-iOS2
//
//  Created by Kostiantyn Sokolinskyi on 3/19/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BRHistoryEntity;

@interface BRDocumentEntity : NSManagedObject

@property (nonatomic) NSDate *createdAt;
@property (nonatomic, retain) NSString * docType;
@property (nonatomic, retain) NSString * externalUrl;
@property (nonatomic, retain) NSString * fileExtension;
@property (nonatomic, retain) NSString * filePath;
@property (nonatomic) int32_t fileSize;
@property (nonatomic, retain) NSString * friendlyName;
@property (nonatomic) int16_t source;
@property (nonatomic, retain) BRHistoryEntity *historyId;

@end
