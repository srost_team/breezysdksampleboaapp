//
//  BRSDKPartnerPrinter.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 23/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BRPrinterEntity.h"
#import "BRPrinterLocationEntity.h"

@interface BRSDKPartnerPrinter : NSObject

@property (nonatomic) BRPrinterEntity *printer;
@property (nonatomic) BRPrinterLocationEntity *printerLocation;

- (instancetype)initWithPrinter: (BRPrinterEntity*)printer
                       location: (BRPrinterLocationEntity*)printerLocation;

@end
