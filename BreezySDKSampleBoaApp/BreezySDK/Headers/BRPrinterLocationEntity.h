//
//  BRPrinterLocationEntity.h
//  Breezy-iOS2
//
//  Created by Alex Zimin on 23/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BRPrinterLocationEntity : NSManagedObject

@property (nonatomic, retain) NSString * addressCity;
@property (nonatomic, retain) NSString * addressCountry;
@property (nonatomic, retain) NSNumber * addressLatitude;
@property (nonatomic, retain) NSString * addressLine1;
@property (nonatomic, retain) NSString * addressLine2;
@property (nonatomic, retain) NSNumber * addressLongitude;
@property (nonatomic, retain) NSString * addressState;
@property (nonatomic, retain) NSString * addressZip;
@property (nonatomic, retain) NSString * availability;
@property (nonatomic, retain) NSNumber * locationId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone;

@end
