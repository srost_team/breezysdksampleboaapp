//
//  BRPrinterSettingsEntity.h
//  Breezy-iOS2
//
//  Created by Kostiantyn Sokolinskyi on 3/19/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BRPrinterSettingsEnums.h"


@interface BRPrinterSettingsEntity : NSManagedObject

@property (nonatomic) EBRPrintColor color;
@property (nonatomic) EBRPrintDuplexing duplexing;
@property (nonatomic) int64_t numberOfCopies;
@property (nonatomic) EBRPrintOrientation orientation;
@property (nonatomic, retain) NSString * pageRange;
@property (nonatomic) BOOL printCoverPage;

@end
