//
//  BRDALoginViewController.m
//  BreezySDKSampleApp
//
//  Created by Stanislav Razbegin on 9/23/15.
//  Copyright (c) 2015 Stanislav Razbegin. All rights reserved.
//

#import "BRDALoginViewController.h"

@interface BRDALoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *ssoEmailTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
- (IBAction)loginClick:(id)sender;

@end

@implementation BRDALoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.activityIndicator.hidden = YES;
    UIBarButtonItem *dismissViewCotnrollerButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(close)];
    self.navigationItem.leftBarButtonItem = dismissViewCotnrollerButton;
}

- (void)close {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)loginClick:(id)sender {
    if (self.ssoEmailTextField.text.length > 0) {
        [self startActivity];
        [self closeKeyboard];
        [[BreezySDK sharedInstance] loginSSOWithEmail:self.ssoEmailTextField.text userGroups:@[] authKey:BR_SSO_KEY success:^{
            [self finishAuthorizationProcessWithSuccess:YES];
        } failure:^(NSError *error) {
            [self finishAuthorizationProcessWithSuccess:NO];
        }];
    }
    else if (self.emailTextField.text.length > 0 && self.passwordTextField.text.length > 0) {
        [self startActivity];
        [self closeKeyboard];
        [[BreezySDK sharedInstance] loginWithEmail:self.emailTextField.text password:self.passwordTextField.text success:^{
            [self finishAuthorizationProcessWithSuccess:YES];
        } failure:^(NSError *error) {
            [self finishAuthorizationProcessWithSuccess:NO];
        }];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Fill appropriate fields before logging in" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)finishAuthorizationProcessWithSuccess:(BOOL)success {
    NSString * message;
    if (success) {
        message = @"You've been successfully logged in";
    }
    else {
        message = @"Login failed, please try again later";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];

    [alert show];
    [self stopActivity];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self close];
}

#pragma mark - Keyboard handling

- (void)closeKeyboard {
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.ssoEmailTextField resignFirstResponder];
}

#pragma mark - Activity indicator

- (void)startActivity {
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
}

- (void)stopActivity {
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
}

@end
