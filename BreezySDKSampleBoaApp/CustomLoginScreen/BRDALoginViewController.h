//
//  BRDALoginViewController.h
//  BreezySDKSampleApp
//
//  Created by Stanislav Razbegin on 9/23/15.
//  Copyright (c) 2015 Stanislav Razbegin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BreezySDK.h"

@interface BRDALoginViewController : UIViewController <UIAlertViewDelegate>

@end
