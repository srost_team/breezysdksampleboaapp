//
//  BRSDKDemoViewController.m
//  Breezy-iOS2
//
//  Created by Alex Zimin on 26/09/14.
//  Copyright (c) 2014 BreezyPrint Corporation. All rights reserved.
//

#import "BRSDKDemoViewController.h"
#import "BRSDKDefaultUIDemoViewController.h"
#import "BRDAMenuViewController.h"
#import "BreezySDK.h"
#import "BRDALoginViewController.h"

@interface BRSDKDemoViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation BRSDKDemoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [BreezySDK initWithBaseURL:[NSURL URLWithString:@"http://api-v20-staging.breezy.com/"]];
}

- (void)viewWillAppear:(BOOL)animated {
    if ( [[BreezySDK sharedInstance] isLoggedIn] ) {
        UIBarButtonItem *debugDeleteDataButton = [[UIBarButtonItem alloc] initWithTitle:@"Log out" style:UIBarButtonItemStyleDone target:self action:@selector(logOut)];
        self.navigationItem.rightBarButtonItem = debugDeleteDataButton;
    } else {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void)logOut {
    [[BreezySDK sharedInstance] logout];
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *title;
    if (indexPath.row == 0) {
        title  = @"Default UI with Default Login Screen";
    }
    else if (indexPath.row == 1) {
        title  = @"Default UI with Custom Login Screen";
    }
    else if (indexPath.row == 2) {
        title  = @"Custom UI with Custom Login Screen";
    }
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.textLabel.text = title;
    
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        // Default UI, default login
        BRSDKDefaultUIDemoViewController *demoVC = [[BRSDKDefaultUIDemoViewController alloc] initWithNibName:@"BRSDKDefaultUIDemoViewController" bundle:nil];
        demoVC.isDefaultLogin = YES;
        UINavigationController *demoVCNav = [[UINavigationController alloc] initWithRootViewController: demoVC];
        [self presentViewController:demoVCNav animated:YES completion:nil];
    }
    else if (indexPath.row == 1) {
        // Default UI, custom login
        BRSDKDefaultUIDemoViewController *demoVC = [[BRSDKDefaultUIDemoViewController alloc] initWithNibName:@"BRSDKDefaultUIDemoViewController" bundle:nil];
        demoVC.isDefaultLogin = NO;
        UINavigationController *demoVCNav = [[UINavigationController alloc] initWithRootViewController: demoVC];
        [self presentViewController:demoVCNav animated:YES completion:nil];
    }
    else if (indexPath.row == 2){
        // Menu of custom UI and custom login
        if ([BreezySDK sharedInstance].isLoggedIn) {
            BRDAMenuViewController *demoVC = [[BRDAMenuViewController alloc] initWithNibName:@"BRDAMenuViewController" bundle:nil];
            UINavigationController *demoVCNav = [[UINavigationController alloc] initWithRootViewController: demoVC];
            [self presentViewController:demoVCNav animated:YES completion:nil];
        }
        else {
            BRDALoginViewController *loginVC = [[BRDALoginViewController alloc] initWithNibName:@"BRDALoginViewController" bundle:nil];
            UINavigationController *loginVCNav = [[UINavigationController alloc] initWithRootViewController: loginVC];
            
            if (IS_IPAD) {
                loginVCNav.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            [self presentViewController:loginVCNav animated:YES completion:nil];
        }
        
    }
}

@end
