//
//  ViewController.m
//  BreezySDKSampleBoaApp
//
//  Created by Stanislav Razbegin on 10/7/15.
//  Copyright (c) 2015 SrostStudio. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
